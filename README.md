# fixit

A simple bash script that handles space and special characters in filenames in your directory for you.

You can just simply create an alias. Add these lines to your .bashrc or .zshrc:

alias fixit='bash PATH_TO_SCRIPT/fix.sh'

You can use another word instead of fixit.

When you run it in x directory, running this script will change every filename in x directory with the proper version of it.

Example: 'Ş?İ AT.pdf' will be 'S_I_AT.pdf'