#!/bin/bash

for f in *; do                                              # do these for every file in the current directory
    n1="$(sed -e "s/[\ <>:\\|!'?\[\]*]\+/_/g" <<< "$f")"    # change special characters and space with '_'
    n2="$(iconv -f utf8 -t ascii//TRANSLIT <<< "$n1")"      # convert UTF-8 to ASCII format 
    if [[ $f != $n2 ]]; then                                # if filename changed, then do
        mv "$f" "$n2";                                      # replace old filename with new filename
    fi
done
